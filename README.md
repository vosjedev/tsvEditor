# TSV Edit
**A simple tsv editor.**

Usage:
```
$ tsvedit --help
usage: tsveditor.py [-h] file

positional arguments:
  file        The file to open. Make sure it is in tsv format.

options:
  -h, --help  show this help message and exit
```

## Todo
This list is made in no particular order. The signals between brackets at the end give an idea of progress. (x) means not started yet, (~) means I am working on it, (t) means it needs to be tested, (p) means work on it is paused and the feature is disabled for now, (v) means done.

- [ ] Improve drawing performace (x)
    - [ ] Don't redraw the whole UI every time (x)
- [x] Adding/removing rows/cols (~)
    - [x] add (~)
        - [x] cols (x)
        - [x] rows (x)
    - [x] remove (x)
        - [x] cols (x)
        - [x] rows (x)
- [x] save without quitting (x)
- [ ] keybind editing (x)
- [ ] option to pin first row of tsv to top line of screen (x)
- [ ] same as above for the first column (x)
- [ ] tabs? (x)
- [x] custom extended tsv format for storing info like column width, options, etc (x)


#!/usr/bin/env python3

# import prompt_toolkit as pt
import os,sys,tty,termios
from math import floor

class Termseq():
    # def __init__(self) -> None:
    #     from subprocess import getoutput
    #     for seq in ['home','smcup','rmcup','clear','rev']
    def __init__(self) -> None:
        import os
        if os.name=='nt':
            print('[termutils.Termseq] windows is not currently supported...')

    # So for now the things here are just called after what I prefer, not the termcap name
    capdb={
        "clear": '\033[H\033[2J\033[3J',
        "cup": '\033[{};{}H',
        "reverse": '\033[7m',
        "underlined": '\033[4m',
        "reset": "\033[0m",
        "smcup": "\033[?1049h",
        "rmcup": "\033[?1049l",
        "sc": "\0337",
        "rc": "\0338",
        "smir": "\033[4h",
        "rmir": "\033[4l",
        "cub": "\033[{}D",
        "cub1": "\x08", # works better for read()
        "cuf": "\033[{}C",
        "cuu": "\033[{}A",
        "cud": "\033[{}B",
        "dch": "\033[{}P",
        "ll": "",
    }
    def pr(self,cap:str,*args,flush:bool=True):
        print(self.capdb[cap].format(*args),end='',flush=flush)
    def cap(self,cap:str,*args):
        return self.capdb[cap].format(*args)

# save term mode
fd = sys.stdin.fileno()
old_settings = termios.tcgetattr(fd)

def getkey():
    try:
       tty.setraw(sys.stdin.fileno())
       ch = sys.stdin.read(1)
    finally:
       termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

class Log():
    def __init__(self):
        self.log=[]
    
    def info(self,component,*msg):
        lmsg=""
        for m in msg:
            lmsg=f"{lmsg} {m}"
        self.log.append(f"[info][{component}]{lmsg}")
    def warn(self,component,*msg):
        lmsg=""
        for m in msg:
            lmsg=f"{lmsg} {m}"
        self.log.append(f"[warn][{component}]{lmsg}")
    def err(self,component,*msg):
        lmsg=""
        for m in msg:
            lmsg=f"{lmsg} {m}"
        self.log.append(f"[err ][{component}]{lmsg}")
    def trow(self):
        for lmsg in self.log:
            print(lmsg)
log=Log()

tsq=Termseq()

def printrow(columns,columnsWidth,selectedField):
    widthLeft=os.get_terminal_size()[0]-1
    colsNotGiven=0
    for cn in range(len(columns)):
        if columnsWidth[cn]<=0:
            colsNotGiven=colsNotGiven+1
        else:
            widthLeft=widthLeft-columnsWidth[cn]
    try:
        fieldMaxLen=floor((widthLeft/colsNotGiven))
        widthLeft=widthLeft%colsNotGiven
    except ZeroDivisionError:
        fieldMaxLen=widthLeft
    # print(colsNotGiven,widthLeft,fieldMaxLen)
    print('|',end='',flush=False)
    for cn in range(len(columns)):
        col=columns[cn]
        if columnsWidth[cn]<=0:
            currentFieldMaxLen=fieldMaxLen
            if widthLeft>0:
                currentFieldMaxLen=currentFieldMaxLen+1
                widthLeft=widthLeft-1
        else:
            currentFieldMaxLen=columnsWidth[cn]
        currentFieldMaxLen=currentFieldMaxLen-1
        # print(currentFieldMaxLen)
        if len(col)>currentFieldMaxLen-1:
            col=f"{col[0:currentFieldMaxLen-2]}…"
        tsq.pr('reset',flush=False)
        if selectedField==cn: tsq.pr('reverse',flush=False)
        print(col,' '*int(currentFieldMaxLen-len(col)-1),end=f"{tsq.cap('reset')}|",flush=False)
    if colsNotGiven==0:
        print(' '*(widthLeft-1)+'|',end='',flush=False)
    print('',flush=False)


    

helptable=[
    ['key'  ,'alt','name'],
    ['q'    ,''   ,'Close table'],
    ['h'    ,''   ,'Toggle help table'],
    ['left' ,''   ,'Move selection left'],
    ['right',''   ,'Move selection right'],
    ['up'   ,''   ,'Move selection up'],
    ['down' ,''   ,'Move selection down'],
    ['e'    ,''   ,'Edit selected cell'],
    ['r'    ,''   ,'Resize selected column'],
    ['a c'  ,'-'  ,'Add a column to the right'],
    ['a r'  ,'\\' ,'Add a row below'],
    ['d c'  ,'  ' ,'Remove selected column'],
    ['d r'  ,'  ' ,'Remove selected row']
]
helptableWidths=[7,7,0]

def viewtable(table,widths):
    from prompt_toolkit import prompt
    from prompt_toolkit.completion import FuzzyCompleter
    
    cols=1
    for i in range(len(table)): # get widest line
        if len(table[i])>cols:
            cols=len(table[i])
    for i in range(len(table)): # adjust all lines to that width
        while len(table[i])<cols:
            table[i].append('')
    
    viewportStart=0
    selected=[0,0]
    noEdits=True
    edited=False
    status='Opened table'
    while True:
        viewportSize=os.get_terminal_size()[1]-5
        tsq.pr('clear')
        editedStr= " s to save |" if edited else ''
        print(f"TSV Editor  |  press h for help | cell ({selected[0]}:{selected[1]}) |{editedStr} {status}"[0:os.get_terminal_size()[0]],flush=False)
        print("."+'-'*(os.get_terminal_size()[0]-2)+".",flush=False)
        for i in range(viewportStart,viewportStart+viewportSize):
            try:
                row=table[i]
            except IndexError:
                break
            sf=-1
            if i==selected[0]:
                sf=selected[1]
            printrow(row,widths,selectedField=sf)
        print("'"+'-'*(os.get_terminal_size()[0]-2)+"'",flush=False)
        print(end='',flush=True)
        noEdits=False
        char=getkey()
        if char=='q':
            if edited:
                print("There are unsaved edits! Close anyway? [y/n]",end='',flush=True)
                yn=getkey()
                if yn=='y':
                    break
                else:
                    status="Save your document first!"
                    continue
            break
        elif char=='h':
            if table==helptable: break
            viewtable(helptable,helptableWidths)
        elif char=='\033':
            print("Waiting for second key... (current chord: ESC )",end='\r')
            char2=getkey()
            if char2=='[':
                noEdits=True
                print("Waiting for third key... (current chord: ESC[ )",end='\r')
                char3=getkey()
                if char3=='A': # up
                    selected[0]=selected[0]-1
                    if selected[0]<0:
                        selected[0]=0
                    if selected[0]<viewportStart:
                        viewportStart=viewportStart-3
                        if viewportStart<0:
                            viewportStart=0
                        status=f"moved viewport up to ({viewportStart}..{viewportStart+viewportSize})"
                elif char3=='B': # down
                    selected[0]=selected[0]+1
                    if selected[0]>=len(table):
                        selected[0]=len(table)-1
                    if selected[0]>=viewportStart+viewportSize:
                        viewportStart=viewportStart+3
                        if viewportStart>len(table):
                            viewportStart=len(table)
                        status=f"moved viewport down to ({viewportStart}..{viewportStart+viewportSize})"
                elif char3=='C': # right
                    selected[1]=selected[1]+1
                    if selected[1]>=len(table[selected[0]]):
                        selected[1]=len(table[selected[0]])-1
                elif char3=='D': # left
                    selected[1]=selected[1]-1
                    if selected[1]<0:
                        selected[1]=0
                else:
                    status=f"Unknown chord ESC[{char3}"
            else:
                noEdits=True
                status=f"Unknown chord ESC{char2}"
        elif char=='e':
            table[selected[0]][selected[1]]=prompt(
                f'({selected[0]}:{selected[1]}) > ',
                default=table[selected[0]][selected[1]]
            )
            status=f"set cell ({selected[0]}:{selected[1]}) to {table[selected[0]][selected[1]]}"
        elif char=='r':
            if not extendedTsvMode: noEdits=True
            w=prompt('Enter a width...')
            if w.isnumeric():
                widths[selected[1]]=int(w)
                status=f"Resized column {selected[1]} to {w}"
            else:
                status=f"Error: {w} is not a number..."
                
        elif char=='s':
            edited=False
            noEdits=True
            try:
                print("Saving...")
                tsvraw=''
                lines=[]
                if extendedTsvMode:
                    nw=[]
                    for w in widths: nw.append(str(w))
                    lines.append('\t'.join(nw))
                for r in tsv:
                    lines.append('\t'.join(r))
                lines.append('')
                tsvraw='\n'.join(lines)

                with open(arg.file.name,'w') as tsvfd:
                    tsvfd.write(tsvraw)
                    tsvfd.flush()
                print("Done.")
                status="Saved."
            except OSError as e:
                status=f"Error saving: {e}"
                
        elif char=='a':
            print("Waiting for second key... (current chord: a )",end='\r')
            char2=getkey()
            if char2=='r':
                nrow=selected[0]+1
                table.insert(nrow,[]) # make new row
                for i in range(cols): # give it columns
                    table[nrow].append('')
                selected[0]=nrow # and focus that row
                if selected[0]>=viewportStart+viewportSize: # fix focus off screen
                    viewportStart=viewportStart+3
                    if viewportStart>len(table):
                        viewportStart=len(table)
                status="Added new row"
            elif char2=='c':
                ncol=selected[1]+1
                for row in range(len(table)):
                    table[row].insert(ncol,'')
                widths.insert(ncol,0)
                cols+=1
                status="Added new column"
            elif char2=='\033':
                status="Cancelled."
                noEdits=True
            else:
                status=f"Unknown chord a {char2}"
                noEdits=True
            
        elif char=='d':
            print("Waiting for second key... (current chord: d )",end='\r')
            char2=getkey()
            if char2=='r':
                if len(table)<=1: # reset when table is one row
                    status="Can't remove row if only one row left, reset row instead."
                    for i in range(len(table[selected[0]])):
                        table[selected[0]][i]=''
                    continue
                table.pop(selected[0]) # remove selected
                if selected[0]>=len(table): # don't select non-existent cells
                    selected[0]=len(table)-1
                status=f"Removed row {selected[0]}"
            elif char2=='c':
                if cols<=1:
                    status="Can't remove column if only one column left!"
                    continue
                for i in range(len(table)):
                    table[i].pop(selected[1])
                cols+=-1
                if selected[1]>cols:
                    selected[1]=cols
            elif char2=='\033':
                status="Cancelled."   
                noEdits=True 
            else:
                status=f"Unknown chord a {char2}"    
                noEdits=True
                    
        elif char=='R':
            for i in range(len(table[selected[0]])):
                table[selected[0]][i]=''
            status=f"Reset row {selected[0]}"
        else:
            status=f"Unknown key '{char}'"
            noEdits=True
        
        if not noEdits:
            edited=True
    
    return table


import argparse
def tsvFileType(fname):
    if not os.path.exists(fname):
        try:
            open(fname,'x').close()
        except OSError as e:
            raise argparse.ArgumentTypeError(f"Can't create new file {fname}!")
    if os.path.exists(fname) and not os.path.isfile(fname):
        raise argparse.ArgumentTypeError(f"Error: {fname} is not a file.")
    if not os.access(fname,os.W_OK):
        print("Can't write to this file! This means you can't save it.")
        yn=input("Do you want to open it anyways? [y/n]")
        if not yn=='y':
            raise argparse.ArgumentTypeError(f"Can't write to file {fname}: Permission denied!")
    return open(fname,'r')
        
argparser=argparse.ArgumentParser()
argparser.add_argument('file',help='The file to open. Make sure it is in tsv format.',type=tsvFileType)


arg=argparser.parse_args()

tsvraw=arg.file.read()

lines=tsvraw.split('\n')
tsv=[]
for rowraw in lines:
    row=rowraw.split('\t')
    if len(row)<=1:
        continue
    tsv.append(row)

extendedTsvMode=False
if arg.file.name.endswith('.etsv'):
    extendedTsvMode=True
    
if len(tsv)<=0: # no empty tables
    tsv=[['']]

widths=[]
if extendedTsvMode:
    widths=tsv.pop(0)
    for i in range(len(widths)):
        widths[i]=int(widths[i])
else:
    for i in range(len(tsv[0])):
        widths.append(0)

tb=None

# enter alternative screen
tsq.pr('smcup')
try: # open table
    tsv=viewtable(tsv,widths)
except Exception:
    import traceback
    tb=traceback.format_exc()
# close
tsq.pr('rmcup')
if tb: print(tb)

log.trow()


